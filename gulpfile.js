var gulp = require("gulp"),
    ugligy = require("gulp-uglify"),
    sass = require("gulp-ruby-sass"),
    livereload = require("gulp-livereload"), // https://www.youtube.com/watch?v=r5fvdIa0ETk
    imagemin = require("gulp-imagemin"),
    concat = require("gulp-concat"),
    prefix = require("gulp-autoprefixer"); // (https://www.youtube.com/watch?v=v259QplNDKk)

/* console error while watch command running */ //(https://www.youtube.com/watch?v=o24f4imRbxQ)
var errorlog = function(error){
    console.log.bind(error);
    this.emit("end");
}

// Scripts task (https://www.youtube.com/watch?v=Kh4eYdd8O4w)
gulp.task("scripts", function() {
    gulp.src("js/*.js")
        .pipe(ugligy())
        .pipe(gulp.dest("min"));
})

// concat(join) all js file into one js file
.task("concatjs", function () {
    gulp.src("js/*")
        .pipe(concat("minified.js"))
        .pipe(gulp.dest("js/"))
})

// styles task
// gulp-ruby-sass: 1.x (https://www.youtube.com/watch?v=cg7lwX0u-U0)
.task("sass", function() {
    return sass("sass/", {style: 'compressed'})
    .on("error", errorlog)
    .pipe(prefix({Browser : ["last 2 version"]})) // (https://www.youtube.com/watch?v=v259QplNDKk)
    // .on("error", console.error.bind(console)) // (https://www.youtube.com/watch?v=o24f4imRbxQ)
    .pipe(gulp.dest("./css"))
    .pipe(livereload()); //(https://www.youtube.com/watch?v=r5fvdIa0ETk)
})

// Image-min task (https://www.youtube.com/watch?v=oXxMdT7T9qU)
.task("image", function () {
    gulp.src("images/*")
        .pipe(imagemin())
        .pipe(gulp.dest("minimags"));
})

// watch task (https://www.youtube.com/watch?v=0luuGcoLnxM)
.task("watch", function() {
    var server = livereload.listen();
    gulp.watch("js/*.js", ["scripts"]);
    gulp.watch("sass/**/*.scss", ["sass"]);
})

// Default task runner (https://www.youtube.com/watch?v=YBGeJnMrzzE)
.task("default", ["scripts", "sass", "watch"]);